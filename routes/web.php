<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::resource('fabrica/categoria','CategoriaController');
Route::resource('fabrica/producto','ProductoController');
Route::resource('personas/proveedor','ProveedorController');
Route::resource('personas/cliente','ClienteController');
Route::resource('fabrica/materiaPrima','MateriaprimaController');
Route::resource('ventas/domicilio','DomicilioController');
Route::resource('ventas/deposito','DepositobaseController');
Route::resource('ventas/pedido','PedidoController');
Route::resource('ventas/venta', 'VentaController');
Route::resource('fabrica/ventas', 'VentasController');



Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/ventas/pedido/pdf/{id}','PedidoController@pdf');


/*Route::post('/login/custom', [
		'uses' => 'LoginController@login',
		'as' => 'login.custom'
]);

Route::group(['middleware' => 'auth'], function(){
	Route::get('/home', function(){
		return view('home');
	})->name('home');;

	Route::get('/dashboard', function(){
		return view('dashboard');
	})->name('dashboard');
});*/