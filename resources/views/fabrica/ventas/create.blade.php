@extends ('layouts.admin')
@section ('contenido')
<nav class="navbar navbar-inverse">
  <ul class="nav navbar-nav">
      <li><a href="{{ URL::to('fabrica/ventas') }}">Ventas</a></li>
      <li><a href="{{ URL::to('fabrica/ventas/create') }}">Registrar venta</a>
  </ul>
</nav>

<h1>Crear venta</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
  <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<!-- if there are creation errors, they will show here -->
{{!! Html::ul($errors->all()) !!}}

{{ Form::open(array('url' => 'fabrica/ventas')) }}

    <div class="form-group">
        {{ Form::label('productos', 'Productos') }}
        {{ Form::text('productos', Input::old('productos'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('cantidades', 'Cantidades') }}
        {{ Form::text('cantidades', Input::old('cantidades'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('total', 'Total de la venta') }}
        {{ Form::number('total', Input::old('total'), array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Registrar', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

@endsection