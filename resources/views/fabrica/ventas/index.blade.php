@extends ('layouts.admin')
@section ('contenido')
	<nav class="navbar navbar-inverse">
    <ul class="nav navbar-nav">
        <li><a href="{{ URL::to('fabrica/ventas') }}">Ventas</a></li>
        <li><a href="{{ URL::to('fabrica/ventas/create') }}">Registrar venta</a>
    </ul>
</nav>

<h1>Ventas</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>ID</td>
						<td>Productos</td>
						<td>Cantidades</td>
						<td>Total</td>

        </tr>
    </thead>
    <tbody>
    @foreach($ventas as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
						<td>{{ $value->productos }}</td>
						<td>{{ $value->cantidades }}</td>
						<td>{{ $value->total }}</td>


            <td>

                <a class="btn btn-small btn-success" href="#">Ver venta</a>

                <a class="btn btn-small btn-info" href="#">Editar venta</a>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection