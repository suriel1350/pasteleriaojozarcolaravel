@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<h3>Materias Primas <a href="materiaPrima/create"><button class="btn btn-success">Agregar</button></a></h3>
			@include('fabrica.materiaPrima.search')
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-condensed table-hover">
					<thead>
						<th>Id</th>
						<th>Nombre</th>
						<th>Proveedor</th>
						<th>Descripcion</th>
						<th>Precio</th>
						<th>Cantidad</th>
						<th>Total</th>
						<th>Opciones</th>
					</thead>
					@foreach ($materiaPrimas as $mat)
					<tr>
						<td>{{ $mat->idmateriaPrima}}</td>
						<td>{{ $mat->nombre}}</td>
						<td>{{ $mat->proveedor}}</td>
						<td>{{ $mat->descripcion}}</td>
						<td>{{ $mat->precio_compra}}</td>
						<td>{{ $mat->cantidad}}</td>
						<td>{{ $mat->total}}</td>
						<td>
							<a href="{{URL::action('MateriaprimaController@edit',$mat->idmateriaPrima)}}"><button class="btn btn-info">Editar</button></a>
							<a href="" data-target="#modal-delete-{{$mat->idmateriaPrima}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
						</td>
					</tr>
					@include('fabrica.materiaPrima.modal')
					@endforeach
				</table>
			</div>
			{{$materiaPrimas->render()}}
		</div>
	</div>
@endsection