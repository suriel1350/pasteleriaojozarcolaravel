@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Nueva Materia Prima</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>

			{!!Form::open(array('url'=>'fabrica/materiaPrima','method'=>'POST','autocomplete'=>'off'))!!}
			{{Form::token()}}
	<div class="row">
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label>Proveedor</label>
				<select name="pidproveedor" class="form-control selectpicker" id="pidproveedor" data-live-search="true">
					@foreach ($proveedores as $pro)					
						<option value="{{$pro->idproveedor}}">{{$pro->nombre}}</option>
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="nombre">Nombre</label>
				<input type="text" name="pnombre" id="pnombre" class="form-control" placeholder="Nombre...">
			</div>
		</div>
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="descripcion">Descripción</label>
				<input type="text" name="pdescripcion" id="pdescripcion" class="form-control" placeholder="Descripcion de la materia prima...">
			</div>
		</div>
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="precio_compra">Precio</label>
				<input type="number" name="pprecio_compra" id="pprecio_compra" class="form-control" placeholder="Precio de la materia prima...">
			</div>
		</div>
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="cantidad">Cantidad</label>
				<input type="number" name="pcantidad" id="pcantidad" class="form-control" placeholder="Cantidad de la materia prima...">
			</div>
		</div>
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<button type="button" id="bt_add" class="btn btn-primary">Agregar</button>
			</div>
		</div>

		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
			<table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
				<thead style="background-color: #A9D0F5">
					<th>Opciones</th>
					<th>Proveedor</th>
					<th>Nombre</th>
					<th>Descripcion</th>
					<th>Precio</th>
					<th>Cantidad</th>
					<th>Subtotal</th>
				</thead>
				<tfoot>
					<th>TOTAL</th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th><h4 id="total">$: 0.00</h4></th>
				</tfoot>
				<tbody>
					
				</tbody>
			</table>
		</div>

		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" id="guardar"> 
			<div class="form-group">
				<input name="_token" value="{{ csrf_token() }}" type="hidden"></input>
				<button class="btn btn-primary" type="submit">Guardar</button>
				<button class="btn btn-danger" type="reset">Cancelar</button>
			</div>
		</div>

	</div>

			
			
			{!!Form::close()!!}

@push('scripts')
	<script>
		$(document).ready(function(){
			$('#bt_add').click(function(){
				agregar();
			});
		});

		var cont = 0;
		total = 0;
		subtotal = [];
		$("#guardar").hide();

		function agregar()
		{
			idproveedor = $("#pidproveedor").val();
			proveedor = $("#pidproveedor option:selected").text();
			nombre = $("#pnombre").val();
			descripcion = $("#pdescripcion").val();
			precio_compra = $("#pprecio_compra").val();
			cantidad = $("#pcantidad").val();

			if(idproveedor != "" && cantidad != "" && cantidad > 0 && precio_compra != "" && nombre != "")
			{
				subtotal[cont] = (cantidad*precio_compra);
				total = total + subtotal[cont];

				var fila = '<tr class="selected" id="fila' + cont+ '"><td><button type="button" class="btn btn-warning" onclick="eliminar('+cont+');">X</button></td><td><input type="hidden" name="idproveedor[]" value="'+idproveedor+'">'+proveedor+'</td><td><input type="text" name="nombre[]" value="'+nombre+'"></td><td><input type="text" name="descripcion[]"value="'+descripcion+'"></td><td><input type="number" name="precio_compra[]" value="'+precio_compra+'"></td><td><input type="number" name="cantidad[]" value="'+cantidad+'"></td><td>'+subtotal[cont]+'</td></tr>';
				cont++;
				limpiar();
				$("#total").html("$: " + total);
				evaluar();
				$("#detalles").append(fila);
			}
			else
			{
				alert("Error, revise los datos del articulo");
			}
		}

		function limpiar()
		{
			$("#pnombre").val("");
			$("#pdescripcion").val("");
			$("#pprecio_compra").val("");
			$("#pcantidad").val("");
		}

		function evaluar()
		{
			if(total>0)
			{
				$("#guardar").show();
			}
			else
			{
				$("#guardar").hide();
			}
		}

		function eliminar(index)
		{
			total = total-subtotal[index];
			$("#total").html("$: " + total);
			$("#fila" + index).remove();
			evaluar();
		}
	</script>
@endpush

@endsection