@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Editar material: {{$materiaPrima->nombre}}</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>

			{!!Form::model($materiaPrima,['method'=>'PATCH','route'=>['materiaPrima.update',$materiaPrima->idmateriaPrima],'files'=>'true'])!!}
			{{Form::token()}}
	<div class="row">
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="nombre">Nombre</label>
				<input type="text" name="nombre" required value="{{$materiaPrima->nombre}}" class="form-control">
			</div>
		</div>
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label>Proveedor</label>
				<select name="idproveedor" class="form-control">
					@foreach ($proveedores as $pro)					
						@if ($pro->idproveedor==$materiaPrima->idproveedor)
						<option value="{{$pro->idproveedor}}" selected>{{$pro->nombre}}</option>
						@else
						<option value="{{$pro->idproveedor}}">{{$pro->nombre}}</option>
						@endif
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="descripcion">Descripción</label>
				<input type="text" name="descripcion" value="{{$materiaPrima->descripcion}}" class="form-control" placeholder="Descripcion del artículo...">
			</div>
		</div>
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="precio_compra">Precio</label>
				<input type="text" name="precio_compra" required value="{{$materiaPrima->precio_compra}}" class="form-control">
			</div>
		</div>
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="cantidad">Cantidad</label>
				<input type="text" name="cantidad" required value="{{$materiaPrima->cantidad}}" class="form-control" placeholder="Cantidad de la materia prima...">
			</div>
		</div>
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div class="form-group">
				<button class="btn btn-primary" type="submit">Guardar</button>
				<button class="btn btn-danger" type="reset">Cancelar</button>
			</div>
		</div>
	</div>

			{!!Form::close()!!}
@endsection