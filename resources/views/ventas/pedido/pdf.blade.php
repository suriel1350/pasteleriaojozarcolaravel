<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: medium;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: medium;
    }
    .gray {
        background-color: #84EB9D;
    }
    .miColor {
      color: #17c1bb;
    }
    .lineaAbajo {
      border-bottom-style: double; 
      border-bottom-width: 1px;
      border-bottom-color: #17c1bb;
    }
    .tamDatos {
      font-weight: bold;
      font-size: x-small;
    }
</style>

</head>
<body>

  <table class="tamDatos" width="100%">
    <tr>
        <td valign="top"><img src="img/Oficial.png" alt="" width="150"/></td>
        <td align="right">
            <h1 class="miColor">Nota de venta {{$pedido->nota_num}}</h1>
            <pre>
                LIDIA MENDEZ PEREZ
                R.F.C MEPL6008269N7
                CURP MEPL600826MVZNRD07
                Prolongación de Juárez No.2
                Ojo Zarco, Nogales, Ver. C.P. 94720
                Tel: (272) 72 6 84 18
            </pre>
        </td>
    </tr>

  </table>

  <br/><br/>

  <table class="tamDatos" width="100%">
    <tr>
        <td class="lineaAbajo"><h2><strong class="miColor">Datos del Cliente</strong></h2></td>
    </tr>

    <tbody>
      <tr>
          <td><h3><strong class="miColor">Nombre: </strong>{{$pedido->cliente}}</h3></td>
      </tr>
      <tr>
          <td><h3><strong class="miColor">Teléfono: </strong>{{$pedido->telefono}}</h3></td>
      </tr>
      <tr>
          <td><h3><strong class="miColor">Fecha de pedido: </strong>{{$pedido->fecha_pedido}}</h3></td>
      </tr>
    </tbody>
  </table>

  <br/>

  <table width="100%">
    <thead style="background-color: #84EB9D;">
      <tr>
        <th>Kilos</th>
        <th>Descripción del Pedido</th>                
        <th>Subtotal $</th>
      </tr>
    </thead>
    <tbody style="background-color: #F0F0F0;">
      <tr>
        <th scope="row">{{$pedido->kilos}}</th>
        <td>{{$pedido->tipo_pastel}} cubierto con {{$pedido->cubierto}}, envinado: {{$pedido->envinado}}, sabor: {{$pedido->sabor}}, relleno: {{$pedido->relleno}}</td>                
        <td align="right">1400.00</td>
      </tr>
     
      <tr>
          <th></th>
          <td><br/>Detalles decorado: {{$pedido->detalles_dec}}, color: {{$pedido->color}}</td>                    
          <td></td>
      </tr>

      <tr>
          <th></th>
          <td><br/>Escribir: {{$pedido->escrito}}</td>          
          <td></td>
      </tr>
      <tr>
          <th></th>
          <td><br/>Tipo de base: {{$pedido->tipo_base}}</td>
          <td></td>
      </tr>
      <tr>
          <th></th>
          <td><br/>Fecha de Entrega: {{$pedido->fecha_entrega}}, hora: {{$pedido->hora_entrega}}</td>                  
          <td></td>
      </tr>
      <tr>
          <th></th>
          <td><br/><br/><br/>Domicilio: {{$pedido->ciudad}}</td>                    
          <td align="right">{{$pedido->precio}}</td>        
      </tr>
      <tr>
          <th> </th>
          <td> </td>                    
          <td> </td>
      </tr>
    </tbody>

    <tfoot>
        <tr>
            <td colspan="1"></td>
            <td align="right">Total $</td>
            <td align="right">{{$pedido->total_pedido}}</td>
        </tr>
        <tr>
            <td colspan="1"></td>
            <td align="right">Anticipo $</td>
            <td align="right">{{$pedido->anticipo}}</td>
        </tr>
        <tr>
            <td colspan="1"></td>
            <td align="right">Resta $</td>
            <td align="right" class="gray">{{$pedido->resta}}</td>
        </tr>
    </tfoot>
  </table>

</body>
</html>