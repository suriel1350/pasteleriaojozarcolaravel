@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Nuevo Pedido </h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>

			{!!Form::open(array('url'=>'ventas/pedido','method'=>'POST','autocomplete'=>'off'))!!}
			{{Form::token()}}
	<div class="row">
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="nota_num">Nota #: </label>
				<input id="pidNota" type="text" name="nota_num"  value="{{old('nota_num')}}" class="form-control" placeholder="Número de la nota...">
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="idcliente">Cliente Registrado</label>
				<select name="idcliente" id="pidCliente" class="form-control selectpicker" data-live-search="true">
					@foreach($clientes as $cliente)
						<option value="{{$cliente->idcliente}}">{{$cliente->nombre}}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="cliente">Cliente no registrado</label>
				<input type="text" id="pidclienteNoReg" name="cliente" value="{{old('cliente')}}" class="form-control" placeholder="Escribe el nombre...">
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="telefono">Teléfono</label>
				<input type="text" name="telefono" id="pidTelefono"  value="{{old('telefono')}}" class="form-control" placeholder="Número de teléfono...">
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="fecha_pedido">Fecha pedido</label>
				<input type="date" name="fecha_pedido" id="pidFechaPedido"  value="{{old('fecha_pedido')}}" class="form-control">
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label>Kilos</label>
				<select name="kilos" id="pidKilos" class="form-control">
					<option value="0.25">Cuarto</option>
					<option value="0.50">Medio</option>
					<option value="0.75">3 Cuartos</option>
					<option value="1">1</option>
					<option value="1.5">1 1/2</option>
					<option value="2">2</option>
					<option value="2.5">2 1/2</option>
					<option value="3">3</option>
					<option value="3.5">3 1/2</option>
					<option value="4">4</option>
					<option value="4.5">4 1/2</option>
					<option value="5">5</option>
					<option value="5.5">5 1/2</option>
					<option value="6">6</option>
					<option value="6.5">6 1/2</option>					
					<option value="7">7</option>
					<option value="7.5">7 1/2</option>					
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
				</select>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label>Tipo pastel</label>
				<select name="tipo_pastel" id="pidTipoPastel" class="form-control">
					<option value="Normal">Normal</option>
					<option value="3 leches">Tres leches</option>
					<option value="4 estaciones">4 Estaciones</option>
				</select>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label>Cubierto con:</label>
				<select name="cubierto" id="pidCubierto" class="form-control">
					<option value="Merengue">Merengue</option>
					<option value="Chantilli">Chantilli</option>
				</select>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label>Envinado</label>
				<select name="envinado" id="pidEnvinado" class="form-control">
					<option value="Si">Si</option>
					<option value="No">No</option>
					<option value="Poco">Poco</option>
				</select>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label>Relleno</label>
				<select name="relleno" id="pidRelleno" class="form-control">
					<option value="No">No</option>
					<option value="Durazno">Durazno</option>
					<option value="Piña">Piña</option>
					<option value="Crema Pas Chocolate">Crema Pas. Chocolate</option>
					<option value="Crema Pas Vainilla">Crema Pas. Vainilla</option>
				</select>
			</div>
		</div>
		
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label>Sabor</label>
				<select name="sabor" id="pidSabor" class="form-control">
					<option value="Vainilla">Vainilla</option>
					<option value="Chocolate">Chocolate</option>
					<option value="Vainilla/Chocolate">Vainilla con chocolate</option>
					<option value="Coco/Piña">Coco con piña</option>
					<option value="Vainilla/pasas">Vainilla con pasas</option>
					<option value="Vainilla/nuez">Vainilla con nuez</option>
				</select>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label>Tipo decorado</label>
				<select name="tipo_decorado" id="pidDecorado" class="form-control">
					<option value="Sencillo">Sencillo</option>
					<option value="Especial">Especial</option>
				</select>
			</div>
		</div>

		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="detalles_dec">Detalles decorado</label>
				<input type="text" name="detalles_dec" id="pidDetallesDec"  value="{{old('detalles_dec')}}" class="form-control" placeholder="Escribe los detalles del decorado...">
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="costo_dec">Costo decorado (kilo)</label>
				<input type="number" name="costo_dec" id="pidCostoDec"  value="{{old('costo_dec')}}" class="form-control" placeholder="Escribe el costo...">
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="color">Color</label>
				<input type="text" name="color" id="pidColor"  value="{{old('color')}}" class="form-control" placeholder="Color del pastel...">
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="escrito">Escribir</label>
				<input type="text" name="escrito" id="pidEscrito"  value="{{old('escrito')}}" class="form-control" placeholder="Escribir en el pastel...">
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label>Depósito Tabla</label>
				<select name="iddeposito" class="form-control selectpicker" id="pidDeposito" data-live-search="true">
					@foreach($depositos as $deposito)
						<option value="{{$deposito->iddeposito}}_{{$deposito->tipo_base}}_{{$deposito->costo}}">{{$deposito->base}}</option>
					@endforeach
				</select>
			</div>					
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="fecha_entrega">Fecha Entrega</label>
				<input type="date" name="fecha_entrega" id="pidFechaEntrega"  value="{{old('fecha_entrega')}}" class="form-control">
			</div>
		</div>


		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="hora_entrega">Hora Entrega</label>
				<input type="time" name="hora_entrega" id="pidHora"  value="{{old('hora_entrega')}}" class="form-control">
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="hora_entrega">Domicilio</label>
				<select id="pidciudad" name="iddomicilio" onchange="onSelectChange()" class="form-control">
					@foreach($domicilios as $domicilio)
						<option value="{{$domicilio->iddomicilio}}_{{$domicilio->ciudad}}_{{$domicilio->precio}}">{{$domicilio->ciudad}}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="costo">Costo</label>
				<input type="number" disabled id="pcosto" class="form-control" placeholder="Precio">
			</div>
		</div>

		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="direccion">Dirección</label>
				<input type="text" disabled="disabled" name="direccion" id="pdireccion" class="form-control" placeholder="Escribe la dirección...">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="panel panel-primary">
			<div class="panel-body">
				<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
					<div class="form-group">
						<label>Producto</label>
						<select name="pidproducto" class="form-control selectpicker" id="pidproducto" data-live-search="true">
							@foreach($productos as $producto)
								<option value="{{$producto->idproducto}}_{{$producto->nombre}}_{{$producto->precio_venta}}">{{$producto->nombre}}</option>
							@endforeach
						</select>
					</div>					
				</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<div class="form-group">
						<label for="cantidad">Cantidad</label>
						<input type="number" name="pcantidad" id="pcantidad" class="form-control" placeholder="Cantidad">
					</div>
				</div>
				<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
					<div class="form-group">
						<label for="precio_venta">Precio Venta</label>
						<input type="number" disabled name="pprecio_venta" id="pprecio_venta" class="form-control" placeholder="Precio_venta">
					</div>
				</div>
				<div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
					<div class="form-group">
						<button type="button" id="bt_add" class="btn btn-primary">Agregar</button>
					</div>
				</div>
				<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
					<table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
						<thead style="background-color: #A9D0F5">
							<th class="text-center col-lg-3 col-sm-3 col-md-3 col-xs-12">Opciones</th>
							<th class="text-center">Producto</th>
							<th class="text-center">Cantidad</th>
							<th class="text-center">Precio</th>
							<th class="text-center">Subtotal</th>
						</thead>
						<tfoot>
							<th>TOTAL PRODUCTOS</th>
							<th></th>
							<th></th>
							<th></th>
							<th><h4 id="total">$ 0.00</h4><input type="hidden" name="total_venta" id="total_venta"></th>
						</tfoot>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12"> 
			<div class="form-group">
				<input name="_token" value="{{ csrf_token() }}" type="hidden"></input>
				<button class="btn btn-success" type="button" id="creaNota">Obtener Total</button>
			</div>
		</div>
	</div>	

	<div class="row" id="guardarNota">
		<div class="panel panel-primary">
			<div class="panel-body">
				<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
					<table id="resumenPedido" class="table table-striped table-bordered table-condensed table-hover">
						<thead style="background-color: #84EB9D">
							<th class="text-center col-lg-2 col-sm-2 col-md-2 col-xs-12">Opciones</th>
							<th class="text-center col-lg-8 col-sm-8 col-md-8 col-xs-12">Detalles</th>
							<th class="text-center">Subtotal</th>
						</thead>
						<tfoot>
							<th>TOTAL PEDIDO</th>
							<th></th>
							<th><h4 id="pidtotPedido">$ 0.00</h4><input type="hidden" name="total_pedido" id="total_pedido"></th>
						</tfoot>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
			<div class="text-center col-lg-3 col-sm-3 col-md-3 col-xs-12">
				<div class="form-group">
					<label for="cantidad">Anticipo</label>
					<input type="number" id="pidanticipo" name="anticipo" class="form-control" placeholder="Cantidad">
				</div>
			</div>

			<div class="text-center col-lg-3 col-sm-3 col-md-3 col-xs-12">
				<div class="form-group">
					<label for="cantidad">Resta</label>
					<input type="number" id="pidresta" name="resta" class="form-control">
				</div>
			</div>

		</div>
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12"> 
			<div class="form-group">
				<input name="_token" value="{{ csrf_token() }}" type="hidden"></input>
				<button class="btn btn-success" type="button" id="pagar">Pagar</button>	
				<button class="btn btn-primary" type="submit">Guardar</button>
				<button class="btn btn-danger" type="reset">Cancelar</button>
			</div>
		</div>
	</div>		
			{!!Form::close()!!}
@push('scripts')
	<script>
		$(document).ready(function(){
			$('#bt_add').click(function(){
				agregar();
			});

			$('#creaNota').click(function(){
				agregaNota();
			});

			$('#pagar').click(function(){
				calculaCambio();
			});
		});

		$(document).on('ready',function(){
  			$('select[name=pidciudad]').val(1);
  			$('.selectpicker').selectpicker('refresh')
  			mostrarValores();
  			mostrarValoresDos();
		});

		$("#pidciudad").change(mostrarValores);
		$("#pidproducto").change(mostrarValoresDos);

		function mostrarValores()
		{
			datosDomicilio=document.getElementById('pidciudad').value.split('_');
			$("#pcosto").val(datosDomicilio[2]);
		}

		function mostrarValoresDos()
		{			
			datosProducto=document.getElementById('pidproducto').value.split('_');
			$("#pprecio_venta").val(datosProducto[2]);
		}

		function calculaCambio()
		{
			cantidadAnticipo = $("#pidanticipo").val();

			if(cantidadAnticipo > totalPedido)
				alert("Introduce en el anticipo una cantidad menor del total del pedido");
			else
			{
				pendientePago = totalPedido - cantidadAnticipo;
				$("#pidresta").val(pendientePago);
			}
		}

		var cont = 0;
		var contDos = 0;
		total = 0;
		totalPedido = 0;
		totalPedidoSinDepo = 0;
		totalPedidoSolo = 0;
		auxTotalPedido = 0;
		sumPedido = 0;
		auxCostoDec = [];
		subtotal = [];
		$("#guardarNota").hide();

		function agregaNota()
		{
			$("#guardarNota").show();
			numeroNota = $("#pidNota").val();
			datosCliente = $("#pidCliente option:selected").text();
			clienteNoRegistrado = $("#pidclienteNoReg").val();
			
			fechaPedido = $("#pidFechaPedido").val();
			kilos = $("#pidKilos option:selected").val();
			kilosTexto = $("#pidKilos option:selected").text();
			tipoPastel = $("#pidTipoPastel option:selected").text();
			cubierto = $("#pidCubierto option:selected").text();
			envinado = $("#pidEnvinado option:selected").text();
			relleno = $("#pidRelleno option:selected").text();
			sabor = $("#pidSabor option:selected").text();
			tipoDecorado = $("#pidDecorado option:selected").text();
			detallesDec = $("#pidDetallesDec").val();
			costoDec = $("#pidCostoDec").val();
			color = $("#pidColor").val();
			escrito = $("#pidEscrito").val();

			datosDeposito=document.getElementById('pidDeposito').value.split('_');
			iddeposito = datosDeposito[0];
			tipoBase = datosDeposito[1];
			costoDeposito = datosDeposito[2];

			fechaEntrega = $("#pidFechaEntrega").val();
			hora = $("#pidHora").val();
			
			ciudad = $("#pidciudad option:selected").text();
			costo = $("#pcosto").val();
			direccion = $("#pdireccion").val();

			/*if(numeroNota != "" && telefonoCliente != "" && fechaPedido != "" && costoDec != "" && color != "" && escrito != "" && fechaEntrega != "" && )
			{*/		
				if(kilos > 1)
					kilosTexto = kilosTexto + " kilos";

				if(kilos == 1)
					kilosTexto = kilosTexto + " kilo";

				if(detallesDec == "")
					detallesDec = "ninguno";

				if(envinado != "")
					envinado = envinado + " envinado";

				if(tipoPastel == "Tres leches")
				{
					envinado = "";
					totalPedido = kilos*740;
				}

				if(tipoPastel == "Normal")
				{
					totalPedido = kilos*400;
				}

				if(tipoPastel == "4 Estaciones")
				{
					totalPedido = kilos*1200;
				}

				if(cubierto == "Chantilli" && tipoPastel != "Tres leches")
					totalPedido = totalPedido + (kilos*100);

				if(relleno != "No")
					totalPedido = totalPedido + (kilos*100);

				if(sabor == "Vainilla con nuez")
					totalPedido = totalPedido + (kilos*100);

				/*if(domicilio == "Si")
				{
					totalPedidoSinDomi = totalPedido;
					totalPedido = totalPedido + (1 * costo);
				}
				if(domicilio == "No")
				{
					totalPedidoSinDomi = totalPedido;
					costo = 0;
				}*/

				totalPedido = totalPedido + (kilos * (costoDec));
				
				totalPedidoSolo = totalPedido;
				totalPedido = totalPedido + (1 * costoDeposito);
				totalPedido = totalPedido + (1 * costo);
				
				auxTotalPedido = totalPedido;

				if(total > 0)
					totalPedido = totalPedido + (1*total);

				var filaPedido = '<tr class="selected" id="filaPedido' + contDos+ '"><td><button type="button" class="btn btn-warning" onclick="eliminarPedido('+contDos+');">Borrar</button></td><td><input type="hidden">'+kilosTexto+' '+tipoPastel+' cubierto con '+cubierto+' &nbsp&nbsp&nbsp'+envinado+'&nbsp&nbsp&nbsp Sabor: '+sabor+' &nbsp&nbsp&nbsp Relleno '+relleno+' <br> Tipo decorado '+tipoDecorado+' &nbsp&nbsp&nbsp Detalles decorado: '+detallesDec+' &nbsp&nbsp&nbsp Color: '+color+' &nbsp&nbsp&nbsp Escribir: '+escrito+'</td><td><input type="hidden">'+totalPedidoSolo+'</td></tr><tr id="filaDeposito' + contDos+ '"><td></td><td>Depósito de base: '+tipoBase+'</td><td>'+costoDeposito+'</td></tr><tr id="filaDomicilio' + contDos+ '"><td></td><td>Domicilio: '+ciudad+'</td><td>'+costo+'</td></tr>';

				$("#pidtotPedido").html("$ " + totalPedido);
				$("#total_pedido").val(totalPedido);
				
				$("#resumenPedido").append(filaPedido);	
				
			/*}
			else
			{
				alert("Error, revise los datos del pedido");
			}*/
		}

		function agregar()
		{
			datosProductoDos=document.getElementById('pidproducto').value.split('_');

			idproducto = datosProductoDos[0];
			producto = $("#pidproducto option:selected").text();
			cantidad = $("#pcantidad").val();
			precio_venta = $("#pprecio_venta").val();

			if(idproducto != "" && cantidad != "" && cantidad > 0 && precio_venta != "")
			{
				
					subtotal[cont] = (cantidad*precio_venta);
					total = total + subtotal[cont];

					var fila = '<tr class="selected" id="fila' + cont+ '"><td><button type="button" class="btn btn-warning" onclick="eliminar('+cont+');">X</button></td><td><input type="hidden" name="idproducto[]" value="'+idproducto+'">'+producto+'</td><td><input type="number" name="cantidad[]" value="'+cantidad+'"></td><td><input type="number" name="precio_venta[]" value="'+precio_venta+'"></td><td>'+subtotal[cont]+'</td></tr>';
					
					cont++;
					limpiar();

					totalPedido = auxTotalPedido + (1 * total);
					$("#total").html("$ " + total);
					$("#total_venta").val(total);

					$("#pidtotPedido").html("$ " + totalPedido);
					$("#total_pedido").val(totalPedido);
					
					$("#detalles").append(fila);	
					$('select[name=pidproducto]').val(1);
        			$('.selectpicker').selectpicker('refresh')
        			mostrarValoresDos();				
			}
			else
			{
				alert("Error al ingresar los productos, revise los datos de cada producto");
			}
		}

		function onSelectChange()
		{
    		var sel = document.getElementById('pidciudad');
    		var strUser = sel.options[sel.selectedIndex].text;

    		/*if(strUser == 'Si')
    		{	 
         		//document.getElementById('pidciudad').disabled = false;  
         		document.getElementById('pdireccion').disabled = false;  
    		}*/

    		if(strUser == 'Ninguna')
    		{	 
         		//document.getElementById('pidciudad').disabled = true;  
         		document.getElementById('pdireccion').disabled = true;  
    		}   
    		else
         		document.getElementById('pdireccion').disabled = false;  

		}

		function limpiar()
		{
			$("#pcantidad").val("");
			$("#pprecio_venta").val("");
		}

		function eliminar(index)
		{
			totalPedido = totalPedido - subtotal[index];
			total = total-subtotal[index];
			
			$("#total").html("$ " + total);
			$("#total_venta").val(total);
			$("#pidtotPedido").html("$ " + totalPedido);
			$("#total_pedido").val(totalPedido);
			$("#fila" + index).remove();
		}

		function eliminarPedido(index)
		{
			$("#filaPedido" + index).remove();
			$("#filaDeposito" + index).remove();
			$("#filaDomicilio" + index).remove();
			$("#guardarNota").hide();
			totalPedido = 0;
		}
	</script>
@endpush

@endsection