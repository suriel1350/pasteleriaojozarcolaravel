@extends ('layouts.admin')
@section ('contenido')

	<div class="row">
		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="proveedor">Número Nota</label>
				<p>{{$pedido->nota_num}}</p>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="proveedor">Cliente</label>
				<p>{{$pedido->cliente}}</p>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="proveedor">Teléfono</label>
				<p>{{$pedido->telefono}}</p>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label>Fecha y hora de entrega</label>
				<p>{{$pedido->fecha_entrega}} - {{$pedido->hora_entrega}}</p>
			</div>
		</div>


		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="proveedor">Kilos</label>
				<p>{{$pedido->kilos}}</p>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="proveedor">Tipo pastel</label>
				<p>{{$pedido->tipo_pastel}}</p>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="proveedor">Cubierto con</label>
				<p>{{$pedido->cubierto}}</p>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="proveedor">Envinado</label>
				<p>{{$pedido->envinado}}</p>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="proveedor">Relleno</label>
				<p>{{$pedido->relleno}}</p>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="proveedor">Sabor</label>
				<p>{{$pedido->sabor}}</p>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="proveedor">Detalles decorado</label>
				<p>{{$pedido->detalles_dec}}</p>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="proveedor">Color</label>
				<p>{{$pedido->color}}</p>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="proveedor">Escribir</label>
				<p>{{$pedido->escrito}}</p>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="proveedor">Tabla</label>
				<p>{{$pedido->tipo_base}}</p>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="proveedor">Depósito tabla</label>
				<p>{{$pedido->costo}}</p>
			</div>
		</div>

		<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
			<div class="form-group">
				<label for="proveedor">Domicilio</label>
				<p>{{$pedido->ciudad}}</p>
			</div>
		</div>
		
	</div>
	<div class="row">
		<div class="panel panel-primary">
			<div class="panel-body">
				
				<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
					<table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
						<thead style="background-color: #A9D0F5">
							
							<th>Producto</th>
							<th>Cantidad</th>
							<th>Precio Venta</th>
							<th>Subtotal</th>
						</thead>
						<tfoot>							
							<th></th>
							<th></th>
							<th></th>
							<th><h4 id="total">{{$pedido->total_pedido}}</h4></th>
						</tfoot>
						<tbody>
							@foreach($detalles as $det)
							<tr>
								<td>{{$det->producto}}</td>
								<td>{{$det->cantidad}}</td>
								<td>{{$det->precio_venta}}</td>
								<td>{{$det->cantidad*$det->precio_venta}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>		
			
@endsection