@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<h3>Lista Pedidos <a href="pedido/create"><button class="btn btn-success">Crear Pedido</button></a></h3>
			@include('ventas.pedido.search')
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-condensed table-hover">
					<thead>
						<th>Numero Nota</th>
						<th>Cliente</th>
						<th>Detalles</th>
						<th>Fecha Entrega</th>
						<th>Total</th>
						<th>Anticipo</th>
						<th>Resta</th>
						<th>Estado Pedido</th>
						<th>Opciones</th>
					</thead>
					@foreach ($pedidos as $ped)
					<tr>
						<td>{{ $ped->nota_num}}</td>
			   			<td>{{ $ped->cliente}}</td>
						<td>{{ $ped->kilos.' '.$ped->tipo_pastel.' '.$ped->cubierto.' '.$ped->relleno}}</td>
						<td>{{ $ped->fecha_entrega}}</td>
						<td>{{ $ped->total_pedido}}</td>
						<td>{{ $ped->anticipo}}</td>
						<td>{{ $ped->resta}}</td>
						<td>{{ $ped->estado}}</td>
						<td>
							<a href="{{URL::action('PedidoController@show',$ped->idpedido)}}"><button class="btn btn-primary">Detalles</button></a>
							<a href="{{url('ventas/pedido/pdf/' . $ped->idpedido)}}"><button class="btn btn-success">Imprimir</button></a>
							<a href="" data-target="#modal-delete-{{$ped->idpedido}}" data-toggle="modal"><button class="btn btn-danger">Pagar</button></a>
						</td>
					</tr>
					@include('ventas.pedido.modal')
					@endforeach
				</table>
			</div>
			{{$pedidos->render()}}
		</div>
	</div>
@endsection