@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<h3>Lugares disponibles <a href="domicilio/create"><button class="btn btn-success">Agregar Domicilio</button></a></h3>
			@include('ventas.domicilio.search')
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-condensed table-hover">
					<thead>
						<th>Id</th>
						<th>Ciudad</th>
						<th>Dirección</th>
						<th>Referencias</th>
						<th>Precio</th>
						<th>Opciones</th>
					</thead>
					@foreach ($domicilios as $dom)
					<tr>
						<td>{{ $dom->iddomicilio}}</td>
						<td>{{ $dom->ciudad}}</td>
						<td>{{ $dom->direccion}}</td>
						<td>{{ $dom->referencias}}</td>
						<td>{{ $dom->precio}}</td>
						<td>
							<a href="{{URL::action('DomicilioController@edit',$dom->iddomicilio)}}"><button class="btn btn-info">Editar</button></a>
							<a href="" data-target="#modal-delete-{{$dom->iddomicilio}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
						</td>
					</tr>
					@include('ventas.domicilio.modal')
					@endforeach
				</table>
			</div>
			{{$domicilios->render()}}
		</div>
	</div>
@endsection