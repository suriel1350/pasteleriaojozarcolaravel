@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Editar Base: {{$depositoBase->tipo_base}}</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div> 
			@endif
		</div>
	</div>

			{!!Form::model($depositoBase,['method'=>'PATCH','route'=>['deposito.update',$depositoBase->iddeposito],'files'=>'true'])!!}
			{{Form::token()}}
	<div class="row">
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="tipo_base">Tipo de base</label>
				<input type="text" name="tipo_base" required value="{{$depositoBase->tipo_base}}" class="form-control">
			</div>
		</div>
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="detalles">Detalles</label>
				<input type="text" name="detalles" required value="{{$depositoBase->detalles}}" class="form-control">
			</div>
		</div>
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="costo">Precio</label>
				<input type="text" name="costo" required value="{{$depositoBase->costo}}" class="form-control">
			</div>
		</div>
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div class="form-group">
				<label for="imagen">Imagen</label>
				<input type="file" name="imagen" class="form-control">
				@if (($depositoBase->imagen)!="")
					<img src="{{asset('imagenes/bases/'.$depositoBase->imagen)}}" height="200px" width="200px">
				@endif
			</div>
		</div>
		<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
			<div class="form-group">
				<button class="btn btn-primary" type="submit">Guardar</button>
				<button class="btn btn-danger" type="reset">Cancelar</button>
			</div>
		</div>
	</div>

			{!!Form::close()!!}
@endsection