@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<h3>Lista Depósitos de Base <a href="deposito/create"><button class="btn btn-success">Nuevo Depósito</button></a></h3>
			@include('ventas.deposito.search')
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-condensed table-hover">
					<thead>
						<th>Id</th>
						<th>Tipo de Base</th>
						<th>Detalles</th>
						<th>Precio</th>
						<th>Imagen</th>
						<th>Opciones</th>
					</thead>
					@foreach ($depositoBases as $dep)
					<tr>
						<td>{{ $dep->iddeposito}}</td>
						<td>{{ $dep->tipo_base}}</td>
						<td>{{ $dep->detalles}}</td>
						<td>{{ $dep->costo}}</td>
						<td>
							<img src="{{asset('imagenes/bases/'.$dep->imagen)}}" alt="{{ $dep->tipo_base}}" height="75px" width="75px" class="img-thumbnail">
						</td>
						<td>
							<a href="{{URL::action('DepositobaseController@edit',$dep->iddeposito)}}"><button class="btn btn-info">Editar</button></a>
							<a href="" data-target="#modal-delete-{{$dep->iddeposito}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
						</td>
					</tr>
					@include('ventas.deposito.modal')
					@endforeach
				</table>
			</div>
			{{$depositoBases->render()}}
		</div>
	</div>
@endsection