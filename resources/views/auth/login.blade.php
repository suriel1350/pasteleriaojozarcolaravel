@extends('layouts.app')

@section('content')

        <div class="form">
            <div class="paddingImg"><img src="{{URL::asset('/img/Oficial.png')}}" height="70%" width="70%"/></div>
            
            <form class="login-form" role="form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus placeholder="Correo electrónico"/>
                    
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" name="password" placeholder="Contraseña" required/>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <button type="submit">Acceder</button>
                <p class="messageDos">¿Olvidaste tu contraseña? <a href="{{ route('password.request') }}">Solicitar</a></p>
            </form>
        </div>
@endsection
