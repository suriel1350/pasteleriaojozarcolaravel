@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<h3>Lista de Proveedores <a href="proveedor/create"><button class="btn btn-success">Agregar</button></a></h3>
			@include('personas.proveedor.search')
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-condensed table-hover">
					<thead>
						<th>Id</th>
						<th>Nombre</th>
						<th>Dirección</th>
						<th>Teléfono</th>
						<th>Email</th>
						<th>Opciones</th>
					</thead>
					@foreach ($proveedores as $pro)
					<tr>
						<td>{{ $pro->idproveedor}}</td>
						<td>{{ $pro->nombre}}</td>
						<td>{{ $pro->direccion}}</td>
						<td>{{ $pro->telefono}}</td>
						<td>{{ $pro->email}}</td>
						<td>
							<a href="{{URL::action('ProveedorController@edit',$pro->idproveedor)}}"><button class="btn btn-info">Editar</button></a>
							<a href="" data-target="#modal-delete-{{$pro->idproveedor}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
						</td>
					</tr>
					@include('personas.proveedor.modal')
					@endforeach
				</table>
			</div>
			{{$proveedores->render()}}
		</div>
	</div>
@endsection