<?php

namespace pasteleriaOjoZarco;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'producto';
    protected $primaryKey = 'idproducto';
    public $timestamps = false;

    protected $fillable = [
    	'idcategoria',
    	'nombre',
    	'stock',
    	'descripcion',
    	'imagen',
    	'precio_venta',
    	'estado'
    ];

    protected $guarded = [
    	
    ];
}
