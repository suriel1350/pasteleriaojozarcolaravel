<?php

namespace pasteleriaOjoZarco;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $table='pedido';
    protected $primaryKey='idpedido';

    public $timestamps=false;

    protected $fillable =[
     'iddeposito',
     'iddomicilio',
     'idcliente',
     'nota_num',
     'cliente',
     'telefono',
     'fecha_pedido',
     'kilos',
     'tipo_pastel',
     'relleno',
     'sabor',
     'cubierto',
     'envinado',
     'color',
     'escrito',
     'tipo_decorado',
     'detalles_dec',
     'costo_dec',
     'base',
     'deposito_base',
     'fecha_entrega',
     'hora_entrega',
     'domicilio',
     'ciudad',
     'direccion',
     'costo_domicilio',
     'total_pedido',
     'estado'
    ];
    
    protected $guarded =[
    
    ];
}
