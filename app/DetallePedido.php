<?php

namespace pasteleriaOjoZarco;

use Illuminate\Database\Eloquent\Model;

class DetallePedido extends Model
{
    protected $table='detalle_pedido_producto';
    protected $primaryKey='iddetalle_pedido_producto';
    
    public $timestamps=false;

    protected $fillable =[
     'idpedido',
     'idproducto',
     'cantidad',
     'precio_venta'
    ];

    protected $guarded =[

    ];
}
