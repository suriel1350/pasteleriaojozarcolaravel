<?php

namespace pasteleriaOjoZarco;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    protected $table = 'proveedor';
    protected $primaryKey = 'idproveedor';
    public $timestamps = false;

    protected $fillable = [
     	'nombre',
     	'direccion',
     	'telefono',
     	'email',
     	'estado'
    ];

    protected $guarded = [
    	
    ];
}
