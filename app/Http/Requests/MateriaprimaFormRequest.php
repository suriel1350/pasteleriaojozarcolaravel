<?php

namespace pasteleriaOjoZarco\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MateriaprimaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'idproveedor'=>'required',
            'nombre'=>'required|max:100',
            'descripcion'=>'max:512',
            'precio_compra'=>'required',
            'cantidad'=>'required'
        ];
    }
}
