<?php

namespace pasteleriaOjoZarco\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PedidoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'iddeposito'=>'required',
            
            'cliente'=>'max:50',
            'telefono'=>'max:11',
            
            'kilos'=>'required',
            
            'relleno'=>'max:50',
            
            
            
            'color'=>'max:50',
            'escrito'=>'max:50',
            'tipo_decorado'=>'max:50',
            'detalles_dec'=>'max:100',
            'costo_dec'=>'max:10',
            
            
            'domicilio'=>'max:10',
            'ciudad'=>'max:50',
            'direccion'=>'max:100',
            'costo_domicilio'=>'max:10',
            'total_pedido'=>'max:20'
        ];
    }
}
