<?php

namespace pasteleriaOjoZarco\Http\Controllers;

use Illuminate\Http\Request;

use pasteleriaOjoZarco\Http\Requests;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use pasteleriaOjoZarco\Http\Requests\VentaFormRequest;
use pasteleriaOjoZarco\Ventaa;
use pasteleriaOjoZarco\DetalleVenta;
use DB;

use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;

class VentaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }  

    public function index(Request $request)
    {
        if ($request) 
        {
           $query=trim($request->get('searchText'));
           $ventas=DB::table('venta as v')
            ->join('detalle_venta as dv','v.idventa','=','dv.idventa')
            ->select('v.idventa','v.fecha_hora','v.estado','v.total_venta')
            ->where('v.idventa','LIKE','%'.$query.'%')
            ->orderBy('v.idventa','desc')
            ->groupBy('v.idventa','v.fecha_hora','v.estado','v.total_venta')
            ->paginate(7);
            return view('ventas.venta.index',["ventas"=>$ventas,"searchText"=>$query]);

        }
    }
    public function create()
    {
     $productos = DB::table('producto as pro')
            ->select('pro.idproducto','pro.nombre','pro.stock','pro.precio_venta')
            ->where('pro.estado','=','Activo')
            ->where('pro.stock','>','0')
            ->get();

        return view("ventas.venta.create",["productos"=>$productos]);
    }

     public function store (VentaFormRequest $request)
    {
     try{
         DB::beginTransaction();
         $venta=new Ventaa;
         $venta->total_venta=$request->get('total_venta');
         
         $mytime = Carbon::now('America/Mexico_City');
         $venta->fecha_hora=$mytime->toDateTimeString();
         $venta->estado='A';
         $venta->save();

         $idproducto = $request->get('idproducto');
         $cantidad = $request->get('cantidad');
         $precio_venta = $request->get('precio_venta');

         $cont = 0;

         while($cont < count($idproducto))
         {
             $detalle = new DetalleVenta();
             $detalle->idventa= $venta->idventa; 
             $detalle->idproducto= $idproducto[$cont];
             $detalle->cantidad= $cantidad[$cont];
             $detalle->precio_venta= $precio_venta[$cont];
             $detalle->save();
             $cont=$cont+1;            
         }

         DB::commit();

        }catch(\Exception $e)
        {
           DB::rollback();
        }

        return Redirect::to('ventas/venta');
    }

    public function show($id)
    {
     $venta=DB::table('venta as v')            
            ->join('detalle_venta as dv','v.idventa','=','dv.idventa')
            ->select('v.idventa','v.fecha_hora','v.estado','v.total_venta')
            ->where('v.idventa','=',$id)
            ->first();

        $detalles=DB::table('detalle_venta as d')
             ->join('producto as p','d.idproducto','=','p.idproducto')
             ->select('p.nombre as producto','d.cantidad','d.precio_venta')
             ->where('d.idventa','=',$id)
             ->get();
        return view("ventas.venta.show",["venta"=>$venta,"detalles"=>$detalles]);
    }

    public function destroy($id)
    {
     $venta=Ventaa::findOrFail($id);
        $venta->estado='C';
        $venta->update();
        return Redirect::to('ventas/venta');
    }
}
