<?php
namespace pasteleriaOjoZarco\Http\Controllers;

use Illuminate\Http\Request;

use pasteleriaOjoZarco\Http\Requests;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use pasteleriaOjoZarco\Http\Requests\MateriaprimaFormRequest;
use pasteleriaOjoZarco\Materiaprima;
use DB;

use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;

class MateriaprimaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
    	if($request)
    	{
    		$query = trim($request->get('searchText'));
    		$materiaPrimas = DB::table('materiaPrima as m') 
    		->join('proveedor as p','m.idproveedor','=','p.idproveedor')
    		->select('m.idmateriaPrima','m.nombre','p.nombre as proveedor','m.descripcion','m.precio_compra','m.cantidad','m.total','m.condicion')
            ->where('m.nombre','LIKE','%'.$query.'%')
    		->where ('condicion','=','1')
    		->orderBy('m.idmateriaPrima','desc')
    		->paginate(7);

    		return view('fabrica.materiaPrima.index',["materiaPrimas"=>$materiaPrimas ,"searchText"=>$query]);
    	}
    }

    public function create()
    {
    	$proveedores = DB::table('proveedor')->where('estado','=','Activo')->get();
    	return view("fabrica.materiaPrima.create",["proveedores"=>$proveedores]);
    }

    public function store(MateriaprimaFormRequest $request)
    {
        try 
        {
            DB::beginTransaction();  
            $idproveedor = $request->get('idproveedor');          
        	$nombre = $request->get('nombre');
            $descripcion = $request->get('descripcion');
            $precio_compra = $request->get('precio_compra');
            $cantidad = $request->get('cantidad');

            $cont = 0;

            while($cont < count($idproveedor))
            {
                $materiaPrima=new Materiaprima;
        	    $materiaPrima->idproveedor= $idproveedor[$cont];
                $materiaPrima->nombre= $nombre[$cont];
                $materiaPrima->descripcion= $descripcion[$cont];
                $materiaPrima->precio_compra= $precio_compra[$cont]; 
                $materiaPrima->cantidad=  $cantidad[$cont];
                $materiaPrima->total= $precio_compra[$cont]*$cantidad[$cont];
                $materiaPrima->condicion='1';
                $materiaPrima->save();
                $cont = $cont + 1;
            }

            DB::commit();
        }
        catch(\Exception $e)
        {
           DB::rollback();
        }

        return Redirect::to('fabrica/materiaPrima');
    }

    public function show($id)
    {
    	return view("fabrica.materiaPrima.show",["materiaPrima"=>Materiaprima::findOrFail($id)]);
    }

    public function edit($id)
    {
    	$materiaPrima=Materiaprima::findOrFail($id);
        $proveedores=DB::table('proveedor')->where('estado','=','Activo')->get();
        
        return view("fabrica.materiaPrima.edit",["materiaPrima"=>$materiaPrima,"proveedores"=>$proveedores]);
    }

    public function update(MateriaprimaFormRequest $request,$id)
    {
    	$materiaPrima=Materiaprima::findOrFail($id);

        $materiaPrima->idproveedor=$request->get('idproveedor');
        $materiaPrima->nombre=$request->get('nombre');
        $materiaPrima->descripcion=$request->get('descripcion');
        $materiaPrima->precio_compra=$request->get('precio_compra');
        $materiaPrima->cantidad=$request->get('cantidad');
        $materiaPrima->total=$request->get('precio_compra')*$request->get('cantidad');
        $materiaPrima->condicion='1';

        $materiaPrima->update();

        return Redirect::to('fabrica/materiaPrima');
    }

    public function destroy($id)
    {
    	$materiaPrima=Materiaprima::findOrFail($id);
        $materiaPrima->condicion='0';
        $materiaPrima->update();
        
        return Redirect::to('fabrica/materiaPrima');
    }
}
