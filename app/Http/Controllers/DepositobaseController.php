<?php

namespace pasteleriaOjoZarco\Http\Controllers;

use Illuminate\Http\Request;

use pasteleriaOjoZarco\Http\Requests;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use pasteleriaOjoZarco\Http\Requests\DepositobaseFormRequest;
use pasteleriaOjoZarco\Depositobase;
use DB;

class DepositobaseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
    	if($request) 
    	{
    		$query = trim($request->get('searchText'));
    		$depositoBases = DB::table('depositoBase')->where('tipo_base','LIKE','%'.$query.'%')
    		->where ('condicion','=','1')
    		->orderBy('iddeposito','desc')
    		->paginate(7);

    		return view('ventas.deposito.index',["depositoBases"=>$depositoBases,"searchText"=>$query]);
    	}
    }

    public function create()
    {
    	return view("ventas.deposito.create");
    }

    public function store(DepositobaseFormRequest $request)
    {
    	$depositoBase = new Depositobase;
    	$depositoBase->tipo_base=$request->get('tipo_base');
    	$depositoBase->detalles=$request->get('detalles');
    	$depositoBase->costo=$request->get('costo');
    	$depositoBase->condicion='1';

    	if (Input::hasFile('imagen')){
         $file=Input::file('imagen');
         $file->move(public_path().'/imagenes/bases/',$file->getClientOriginalName());
            $depositoBase->imagen=$file->getClientOriginalName();
        }

    	$depositoBase->save();

    	return Redirect::to('ventas/deposito');
    }

    public function show($id)
    {
    	return view("ventas.deposito.show",["depositoBase"=>Depositobase::findOrFail($id)]);
    }

    public function edit($id)
    {
    	return view("ventas.deposito.edit",["depositoBase"=>Depositobase::findOrFail($id)]);
    }

    public function update(DepositobaseFormRequest $request,$id)
    {
    	$depositoBase=Depositobase::findOrFail($id);

        $depositoBase->tipo_base=$request->get('tipo_base');
        $depositoBase->detalles=$request->get('detalles');
        $depositoBase->costo=$request->get('costo');
        $depositoBase->condicion='1';

        if (Input::hasFile('imagen')){
         $file=Input::file('imagen');
         $file->move(public_path().'/imagenes/bases/',$file->getClientOriginalName());
         $depositoBase->imagen=$file->getClientOriginalName();
        }

        $depositoBase->update();

        return Redirect::to('ventas/deposito');
    }

    public function destroy($id)
    {
    	$depositoBase=Depositobase::findOrFail($id);
    	$depositoBase->condicion='0';
    	$depositoBase->update();

    	return Redirect::to('ventas/deposito');
    }
}
