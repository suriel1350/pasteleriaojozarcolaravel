<?php

namespace pasteleriaOjoZarco\Http\Controllers;

use Illuminate\Http\Request;

use pasteleriaOjoZarco\Http\Requests;
use pasteleriaOjoZarco\Domicilio;
use Illuminate\Support\Facades\Redirect;
use pasteleriaOjoZarco\Http\Requests\DomicilioFormRequest;
use DB;

class DomicilioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
    	if($request)
    	{
    		$query = trim($request->get('searchText'));
    		$domicilios = DB::table('domicilio')->where('ciudad','LIKE','%'.$query.'%')
    		->where ('condicion','=','1')
    		->orderBy('iddomicilio','desc')
    		->paginate(7);

    		return view('ventas.domicilio.index',["domicilios"=>$domicilios,"searchText"=>$query]);
    	}
    }

    public function create()
    {
    	return view("ventas.domicilio.create");
    }

    public function store(DomicilioFormRequest $request)
    {
    	$domicilio = new Domicilio;
    	$domicilio->ciudad=$request->get('ciudad');
    	$domicilio->direccion=$request->get('direccion');
    	$domicilio->referencias=$request->get('referencias');
    	$domicilio->precio=$request->get('precio');
    	$domicilio->condicion='1';
    	$domicilio->save();

    	return Redirect::to('ventas/domicilio');
    }

    public function show($id)
    {
    	return view("ventas.domicilio.show",["domicilio"=>Domicilio::findOrFail($id)]);
    }

    public function edit($id)
    {
    	return view("ventas.domicilio.edit",["domicilio"=>Domicilio::findOrFail($id)]);
    }

    public function update(DomicilioFormRequest $request, $id)
    {
    	$domicilio=Domicilio::findOrFail($id);
    	$domicilio->ciudad=$request->get('ciudad');
    	$domicilio->direccion=$request->get('direccion');
    	$domicilio->referencias=$request->get('referencias');
    	$domicilio->precio=$request->get('precio');
    	$domicilio->update();

    	return Redirect::to('ventas/domicilio');
    }

    public function destroy($id)
    {
    	$domicilio=Domicilio::findOrFail($id);
    	$domicilio->condicion='0';
    	$domicilio->update();

    	return Redirect::to('ventas/domicilio');
    }
}
