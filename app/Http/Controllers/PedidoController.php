<?php

namespace pasteleriaOjoZarco\Http\Controllers;

use Illuminate\Http\Request;

use pasteleriaOjoZarco\Http\Requests;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use pasteleriaOjoZarco\Http\Requests\PedidoFormRequest;
use pasteleriaOjoZarco\Pedido;
use pasteleriaOjoZarco\DetallePedido;
use DB;

use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;
use PDF;

class PedidoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if($request)
        {
            $query=trim($request->get('searchText'));
            $pedidos=DB::table('pedido as p')
            ->join('depositoBase as db','p.iddeposito','=','db.iddeposito')
            ->join('domicilio as d','p.iddomicilio','=','d.iddomicilio')
            
            ->join('cliente as c','p.idcliente','=','c.idcliente')
            ->join('detalle_pedido_producto as dpp','p.idpedido','=','dpp.idpedido')
            ->select('p.idpedido','p.nota_num','p.cliente','p.kilos','p.tipo_pastel','p.cubierto','p.relleno','p.fecha_entrega','p.total_pedido','p.anticipo','p.resta','p.estado')
            ->where('p.cliente','LIKE','%'.$query.'%')
            ->orderBy('p.idpedido','desc')
            ->paginate(7);

            return view('ventas.pedido.index',["pedidos"=>$pedidos,"searchText"=>$query]);
        }
    }

    public function create()
    {
    	$depositos = DB::table('depositoBase as dep')
            ->select(DB::raw('CONCAT(dep.tipo_base, " ",dep.costo) AS base'),'dep.iddeposito','dep.tipo_base','dep.costo')
            ->where('dep.condicion','=','1')
            ->get();
    	$domicilios = DB::table('domicilio as dom')
    		->select('dom.iddomicilio','dom.ciudad','dom.precio')
    		->where('condicion','=','1')
    		->get();
    	$productos = DB::table('producto as pro')
    		->select('pro.idproducto','pro.nombre','pro.precio_venta')
    		->where('estado','=','Activo')
    		->get();

    	$clientes = DB::table('cliente')->where('estado','=','Activo')->get();

    	return view("ventas.pedido.create",["depositos"=>$depositos,"domicilios"=>$domicilios,"productos"=>$productos,"clientes"=>$clientes]);
    }

    public function store (PedidoFormRequest $request)
    {
     
      
         $pedido=new Pedido;         
         $iddeposito=$request->get('iddeposito');
         $pedido->iddeposito=$iddeposito[0];

         $iddomicilio=$request->get('iddomicilio');
         $pedido->iddomicilio=$iddomicilio[0];   

         $pedido->idcliente=$request->get('idcliente');       	
         
         $pedido->nota_num=$request->get('nota_num');
         $pedido->cliente=$request->get('cliente');
         $pedido->telefono=$request->get('telefono');
         $pedido->fecha_pedido=$request->get('fecha_pedido');
         $pedido->kilos=$request->get('kilos');
         $pedido->tipo_pastel=$request->get('tipo_pastel');
         $pedido->relleno=$request->get('relleno');
         $pedido->sabor=$request->get('sabor');
         $pedido->cubierto=$request->get('cubierto');
         $pedido->envinado=$request->get('envinado');
         $pedido->color=$request->get('color');
         $pedido->escrito=$request->get('escrito');
         $pedido->tipo_decorado=$request->get('tipo_decorado');
         $pedido->detalles_dec=$request->get('detalles_dec');
         $pedido->costo_dec=$request->get('costo_dec');         
         $pedido->fecha_entrega=$request->get('fecha_entrega');
         $pedido->hora_entrega=$request->get('hora_entrega');
         
         $direccion=$request->get('direccion');
         if($direccion == '')
            $pedido->direccion='Ninguna';
        else
            $pedido->direccion=$request->get('direccion');
         
         $pedido->total_pedido=$request->get('total_pedido');
         $pedido->anticipo=$request->get('anticipo');
         $pedido->resta=$request->get('resta');
         
         $pedido->estado='Pendiente';
         $pedido->save();       
        
         $idproducto = $request->get('idproducto');
         $cantidad = $request->get('cantidad');
         $precio_venta = $request->get('precio_venta');

         $cont = 0;

         while($cont < count($idproducto))
         {
             $detalle = new DetallePedido();
             $detalle->idpedido= $pedido->idpedido; 
             $detalle->idproducto= $idproducto[$cont];
             $detalle->cantidad= $cantidad[$cont];
             $detalle->precio_venta= $precio_venta[$cont];
             $detalle->save();
             $cont=$cont+1;            
         }
		
		
        return Redirect::to('ventas/pedido');
    }

    public function show($id)
    {
     $pedido=DB::table('pedido as p')
     		->join('depositoBase as db','p.iddeposito','=','db.iddeposito')
            ->join('domicilio as d','p.iddomicilio','=','d.iddomicilio')
            
            ->join('cliente as c','p.idcliente','=','c.idcliente')
            ->join('detalle_pedido_producto as dpp','p.idpedido','=','dpp.idpedido')            
            ->where('p.idpedido','=',$id)
            ->first();

        $detalles=DB::table('detalle_pedido_producto as dpp')
             ->join('producto as p','dpp.idproducto','=','p.idproducto')
             ->select('p.nombre as producto','dpp.cantidad','dpp.precio_venta')
             ->where('dpp.idpedido','=',$id)
             ->get();
        return view("ventas.pedido.show",["pedido"=>$pedido,"detalles"=>$detalles]);
    }

    public function pdf($id)
    {
     $pedido=DB::table('pedido as p')
            ->join('depositoBase as db','p.iddeposito','=','db.iddeposito')
            ->join('domicilio as d','p.iddomicilio','=','d.iddomicilio')
            
            ->join('cliente as c','p.idcliente','=','c.idcliente')
            ->join('detalle_pedido_producto as dpp','p.idpedido','=','dpp.idpedido')            
            ->where('p.idpedido','=',$id)
            ->first();

        $detalles=DB::table('detalle_pedido_producto as dpp')
             ->join('producto as p','dpp.idproducto','=','p.idproducto')
             ->select('p.nombre as producto','dpp.cantidad','dpp.precio_venta')
             ->where('dpp.idpedido','=',$id)
             ->get();
        
        
        
        $pdf = PDF::loadView('ventas.pedido.pdf', ["pedido"=>$pedido,"detalles"=>$detalles]);
        
        return $pdf->download('prueba.pdf');
    }

    public function destroy($id)
    {
     $pedido=Pedido::findOrFail($id);
        $pedido->estado='Pagado';
        $pedido->update();
        return Redirect::to('ventas/pedido');
    }
}
