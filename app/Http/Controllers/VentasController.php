<?php

namespace pasteleriaOjoZarco\Http\Controllers;

use Illuminate\Http\Request;
use pasteleriaOjoZarco\Venta;

use pasteleriaOjoZarco\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Session;

class VentasController extends Controller
{
    public function index(){
        $ventas = Venta::all();

        return \View::make('fabrica.ventas.index')
            ->with('ventas', $ventas);
    }

    public function create(){
        return \View::make('fabrica.ventas.create');
    }

    public function store()
    {
        $rules = array(
            'productos'       => 'required',
            'cantidades'      => 'required',
            'total' => 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('fabrica/ventas/create')
                ->withErrors($validator);
        } else {
            // store
            $venta = new Venta;
            $venta->productos       = Input::get('productos');
            $venta->cantidades      = Input::get('cantidades');
            $venta->total = Input::get('total');
            $venta->save();

            // redirect
            Session::flash('message', 'Venta registrada');
            return Redirect::to('fabrica/ventas');
        }
    }
}
