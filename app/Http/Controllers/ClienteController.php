<?php

namespace pasteleriaOjoZarco\Http\Controllers;

use Illuminate\Http\Request;
use pasteleriaOjoZarco\Http\Requests;
use pasteleriaOjoZarco\Cliente;
use Illuminate\Support\Facades\Redirect;
use pasteleriaOjoZarco\Http\Requests\ClienteFormRequest;
use DB;

class ClienteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }  

    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $clientes=DB::table('cliente')
            ->where('nombre','LIKE','%'.$query.'%')
    		->where ('estado','=','Activo')
            ->orderBy('idcliente','desc')
            ->paginate(7);
            
            return view('personas.cliente.index',["clientes"=>$clientes,"searchText"=>$query]);
        }
    }

    public function create()
    {
        return view("personas.cliente.create");
    }
    
    public function store (ClienteFormRequest $request)
    {
        $cliente=new Cliente;
        $cliente->nombre=$request->get('nombre');
        $cliente->direccion=$request->get('direccion');
        $cliente->telefono=$request->get('telefono');
        $cliente->email=$request->get('email'); 
        $cliente->estado='Activo';       
        $cliente->save();
        
        return Redirect::to('personas/cliente');
    }
    
    public function show($id)
    {
        return view("personas.cliente.show",["cliente"=>Cliente::findOrFail($id)]);
    }
    
    public function edit($id)
    {
        return view("personas.cliente.edit",["cliente"=>Cliente::findOrFail($id)]);
    }

    public function update(ClienteFormRequest $request,$id)
    {
        $cliente=Cliente::findOrFail($id);

        $cliente->nombre=$request->get('nombre');
        $cliente->direccion=$request->get('direccion');
        $cliente->telefono=$request->get('telefono');
        $cliente->email=$request->get('email');

        $cliente->update();
        
        return Redirect::to('personas/cliente');
    }
    
    public function destroy($id)
    {
        $cliente=Cliente::findOrFail($id);
        $cliente->estado='Inactivo';
        $cliente->update();
        
        return Redirect::to('personas/cliente');
    }
}
