<?php

namespace pasteleriaOjoZarco\Http\Controllers;

use Illuminate\Http\Request;
use pasteleriaOjoZarco\Http\Requests;
use pasteleriaOjoZarco\Proveedor;
use Illuminate\Support\Facades\Redirect;
use pasteleriaOjoZarco\Http\Requests\ProveedorFormRequest;
use DB;

class ProveedorController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }  

    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $proveedores=DB::table('proveedor')
            ->where('nombre','LIKE','%'.$query.'%')
    		->where ('estado','=','Activo')
            ->orderBy('idproveedor','desc')
            ->paginate(7);
            
            return view('personas.proveedor.index',["proveedores"=>$proveedores,"searchText"=>$query]);
        }
    }

    public function create()
    {
        return view("personas.proveedor.create");
    }
    
    public function store (ProveedorFormRequest $request)
    {
        $proveedor=new Proveedor;
        $proveedor->nombre=$request->get('nombre');
        $proveedor->direccion=$request->get('direccion');
        $proveedor->telefono=$request->get('telefono');
        $proveedor->email=$request->get('email'); 
        $proveedor->estado='Activo';       
        $proveedor->save();
        
        return Redirect::to('personas/proveedor');
    }
    
    public function show($id)
    {
        return view("personas.proveedor.show",["proveedor"=>Proveedor::findOrFail($id)]);
    }
    
    public function edit($id)
    {
        return view("personas.proveedor.edit",["proveedor"=>Proveedor::findOrFail($id)]);
    }

    public function update(ProveedorFormRequest $request,$id)
    {
        $proveedor=Proveedor::findOrFail($id);

        $proveedor->nombre=$request->get('nombre');
        $proveedor->direccion=$request->get('direccion');
        $proveedor->telefono=$request->get('telefono');
        $proveedor->email=$request->get('email');

        $proveedor->update();
        
        return Redirect::to('personas/proveedor');
    }
    
    public function destroy($id)
    {
        $proveedor=Proveedor::findOrFail($id);
        $proveedor->estado='Inactivo';
        $proveedor->update();
        
        return Redirect::to('personas/proveedor');
    } 
}
