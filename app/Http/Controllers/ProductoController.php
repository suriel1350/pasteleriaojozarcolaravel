<?php

namespace pasteleriaOjoZarco\Http\Controllers;

use Illuminate\Http\Request;
 
use pasteleriaOjoZarco\Http\Requests;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use pasteleriaOjoZarco\Http\Requests\ProductoFormRequest;
use pasteleriaOjoZarco\Producto;
use DB;

class ProductoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
    	if($request)
    	{
    		$query = trim($request->get('searchText'));
    		$productos = DB::table('producto as p')
    		->join('categoria as c','p.idcategoria','=','c.idcategoria')
    		->select('p.idproducto','p.nombre','p.stock','c.nombre as categoria','p.descripcion','p.imagen','p.precio_venta','p.estado')
            ->where('p.nombre','LIKE','%'.$query.'%')
    		->orderBy('p.idproducto','desc')
    		->paginate(7);

    		return view('fabrica.producto.index',["productos"=>$productos,"searchText"=>$query]);
    	}
    }

    public function create()
    {
    	$categorias = DB::table('categoria')->where('condicion','=','1')->get();
    	return view("fabrica.producto.create",["categorias"=>$categorias]);
    }

    public function store(ProductoFormRequest $request)
    {
    	$producto=new Producto;
    	$producto->idcategoria=$request->get('idcategoria');
        $producto->nombre=$request->get('nombre');
        $producto->stock=$request->get('stock');
        $producto->descripcion=$request->get('descripcion');
        $producto->precio_venta=$request->get('precio_venta');
        $producto->estado='Activo';

        if (Input::hasFile('imagen')){
         $file=Input::file('imagen');
         $file->move(public_path().'/imagenes/productos/',$file->getClientOriginalName());
            $producto->imagen=$file->getClientOriginalName();
        }
        
        $producto->save();

        return Redirect::to('fabrica/producto');
    }

    public function show($id)
    {
    	return view("fabrica.producto.show",["producto"=>Producto::findOrFail($id)]);
    }

    public function edit($id)
    {
    	$producto=Producto::findOrFail($id);
        $categorias=DB::table('categoria')->where('condicion','=','1')->get();
        
        return view("fabrica.producto.edit",["producto"=>$producto,"categorias"=>$categorias]);
    }

    public function update(ProductoFormRequest $request,$id)
    {
    	$producto=Producto::findOrFail($id);

        $producto->idcategoria=$request->get('idcategoria');
        $producto->nombre=$request->get('nombre');
        $producto->stock=$request->get('stock');
        $producto->descripcion=$request->get('descripcion');
        $producto->precio_venta=$request->get('precio_venta');
        $producto->estado='Activo';

        if (Input::hasFile('imagen')){
         $file=Input::file('imagen');
         $file->move(public_path().'/imagenes/productos/',$file->getClientOriginalName());
         $producto->imagen=$file->getClientOriginalName();
        }

        $producto->update();

        return Redirect::to('fabrica/producto');
    }

    public function destroy($id)
    {
    	$producto=Producto::findOrFail($id);
        $producto->Estado='Inactivo';
        $producto->update();
        
        return Redirect::to('fabrica/producto');
    }
}
