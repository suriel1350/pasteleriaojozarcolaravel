<?php

namespace pasteleriaOjoZarco;

use Illuminate\Database\Eloquent\Model;

class Ventaa extends Model
{
    protected $table='venta';
    protected $primaryKey='idventa';

    public $timestamps=false;

    protected $fillable =[
     'fecha_hora',
     'total_venta',
     'estado'
    ];
    
    protected $guarded =[
    
    ];
}
