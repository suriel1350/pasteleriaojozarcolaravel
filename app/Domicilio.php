<?php

namespace pasteleriaOjoZarco;

use Illuminate\Database\Eloquent\Model;

class Domicilio extends Model
{
    protected $table = 'domicilio';
    protected $primaryKey = 'iddomicilio';
    public $timestamps = false;

    protected $fillable = [
     	'ciudad',
     	'direccion',
     	'referencias',
     	'precio',
     	'condicion'
    ];

    protected $guarded = [
    	
    ];
}
