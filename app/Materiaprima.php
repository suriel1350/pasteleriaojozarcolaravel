<?php

namespace pasteleriaOjoZarco;

use Illuminate\Database\Eloquent\Model;

class Materiaprima extends Model
{
    protected $table = 'materiaPrima';
    protected $primaryKey = 'idmateriaPrima';
    public $timestamps = false;

    protected $fillable = [
    	'idproveedor',
    	'nombre',
    	'descripcion',
    	'precio_compra',
    	'cantidad',
    	'total',
    	'condicion'
    ];

    protected $guarded = [
    	
    ];
}
