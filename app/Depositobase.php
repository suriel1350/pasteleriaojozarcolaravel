<?php

namespace pasteleriaOjoZarco;

use Illuminate\Database\Eloquent\Model;

class Depositobase extends Model
{
    protected $table = 'depositoBase';
    protected $primaryKey = 'iddeposito';
    public $timestamps = false;

    protected $fillable = [
     	'tipo_base',
     	'detalles',
     	'costo',
     	'imagen',
     	'condicion'
    ];

    protected $guarded = [
    	
    ];
}
