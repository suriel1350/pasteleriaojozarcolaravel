<?php

namespace pasteleriaOjoZarco;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'cliente';
    protected $primaryKey = 'idcliente';
    public $timestamps = false;

    protected $fillable = [
     	'nombre',
     	'direccion',
     	'telefono',
     	'email',
     	'estado'
    ];

    protected $guarded = [
    	
    ];
}
