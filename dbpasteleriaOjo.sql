-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 07-06-2017 a las 03:06:55
-- Versión del servidor: 5.6.26
-- Versión de PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbpasteleriaOjo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `idcategoria` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(256) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `condicion` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`idcategoria`, `nombre`, `descripcion`, `condicion`) VALUES
(1, 'Postre', 'Postres Pequenios', 1),
(2, 'Pastel', 'Normal y 3 leches', 1),
(3, 'Infantil', 'Cumpleaños fiestas', 0),
(4, 'Articulos de fiesta', 'Platos y demas unicel', 0),
(5, 'Gelatina', 'Agua', 0),
(6, 'Fiesta', 'Hola', 1),
(7, 'Infantil', 'hdkhhdshsd', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idcliente` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `direccion` varchar(70) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `estado` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idcliente`, `nombre`, `direccion`, `telefono`, `email`, `estado`) VALUES
(1, 'No existe', 'no', '0', 'no@gmail.com', 'Activo'),
(2, 'Miguel Angel', 'Cordoba', '28282882828', 'miguel@gmail.com', 'Activo'),
(3, 'Heron Zurita', 'Cordoba', '8282828282', 'heron@gmail.com', 'Activo'),
(4, 'Eduardo Herrera', 'Cordoba', '2727272', 'lalo@gmail.com', 'Activo'),
(5, 'Juan', 'Orizaba', '234567881', 'cliente@hotma.com', 'Inactivo'),
(6, 'Ricardo', 'Puebla', '2828282', 'ric@gm.com', 'Inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `depositoBase`
--

CREATE TABLE `depositoBase` (
  `iddeposito` int(11) NOT NULL,
  `tipo_base` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `detalles` varchar(256) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `costo` decimal(11,2) NOT NULL,
  `imagen` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `condicion` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `depositoBase`
--

INSERT INTO `depositoBase` (`iddeposito`, `tipo_base`, `detalles`, `costo`, `imagen`, `condicion`) VALUES
(1, 'Pisos', 'Metalica', '200.00', 'baseUno.jpg', 1),
(2, 'Redondos', '4 postes', '400.00', 'baseTRes.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_pedido_producto`
--

CREATE TABLE `detalle_pedido_producto` (
  `iddetalle_pedido_producto` int(11) NOT NULL,
  `idpedido` int(11) NOT NULL,
  `idproducto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_venta` decimal(11,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `detalle_pedido_producto`
--

INSERT INTO `detalle_pedido_producto` (`iddetalle_pedido_producto`, `idpedido`, `idproducto`, `cantidad`, `precio_venta`) VALUES
(18, 22, 2, 2, '90.00'),
(19, 23, 1, 1, '0.00'),
(20, 24, 4, 10, '8.00'),
(21, 24, 2, 2, '90.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_venta`
--

CREATE TABLE `detalle_venta` (
  `iddetalle_venta` int(11) NOT NULL,
  `idventa` int(11) NOT NULL,
  `idproducto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_venta` decimal(11,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `detalle_venta`
--

INSERT INTO `detalle_venta` (`iddetalle_venta`, `idventa`, `idproducto`, `cantidad`, `precio_venta`) VALUES
(1, 1, 2, 2, '90.00'),
(2, 1, 3, 4, '20.00'),
(3, 2, 2, 1, '90.00'),
(4, 2, 4, 5, '8.00');

--
-- Disparadores `detalle_venta`
--
DELIMITER $$
CREATE TRIGGER `tr_updStockVenta` AFTER INSERT ON `detalle_venta` FOR EACH ROW BEGIN
UPDATE producto SET stock = stock - NEW.cantidad
WHERE producto.idproducto = NEW.idproducto;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilio`
--

CREATE TABLE `domicilio` (
  `iddomicilio` int(11) NOT NULL,
  `ciudad` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `direccion` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `referencias` varchar(256) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `precio` decimal(11,2) NOT NULL,
  `condicion` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `domicilio`
--

INSERT INTO `domicilio` (`iddomicilio`, `ciudad`, `direccion`, `referencias`, `precio`, `condicion`) VALUES
(1, 'Ninguna', 'Ninguna', '', '0.00', 1),
(2, 'Cordoba', 'Avenida Cinco', 'Bar', '200.00', 0),
(3, 'Orizaba', 'Circunvalacion', 'Banco HSBC', '100.00', 1),
(4, 'Ciudad Mendoza', 'Esfuerzo', NULL, '50.00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materiaPrima`
--

CREATE TABLE `materiaPrima` (
  `idmateriaPrima` int(11) NOT NULL,
  `idproveedor` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(256) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `precio_compra` decimal(11,2) NOT NULL,
  `cantidad` decimal(11,2) NOT NULL,
  `total` decimal(11,2) NOT NULL,
  `condicion` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `materiaPrima`
--

INSERT INTO `materiaPrima` (`idmateriaPrima`, `idproveedor`, `nombre`, `descripcion`, `precio_compra`, `cantidad`, `total`, `condicion`) VALUES
(1, 5, 'Harina', 'Hoja de Plata', '600.00', '2.00', '1200.00', 1),
(2, 2, 'Mantequilla', 'Carrancedo', '789.00', '3.00', '2367.00', 0),
(3, 5, 'Azucar', 'Morena', '450.75', '7.00', '3155.25', 1),
(4, 2, 'Harina', 'hhaha', '65.00', '5.00', '325.00', 0),
(5, 2, 'Jamon', 'Pavo', '45.00', '4.00', '180.00', 1),
(6, 2, 'Mantequilla', 'Carrancedo', '650.00', '7.00', '4550.00', 1),
(7, 2, 'Velas', 'Marvel', '34.00', '4.00', '136.00', 1),
(8, 2, 'Vaso', 'Mediano', '34.56', '2.00', '69.12', 1),
(9, 2, 'Gelatina', 'Bolsa', '13.00', '4.00', '52.00', 0),
(10, 5, 'Azucar', 'Morena', '600.00', '7.00', '4200.00', 1),
(11, 2, 'jsjasj', 'sasas', '100.00', '2.00', '200.00', 1),
(12, 2, 'jajc', 'ccsdcds', '300.00', '6.00', '1800.00', 1),
(13, 2, 'dasdsad', 'dasdsada', '150.00', '4.00', '600.00', 1),
(14, 3, 'Mantequilla', 'dajdasd', '230.00', '1.00', '230.00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE `pedido` (
  `idpedido` int(11) NOT NULL,
  `iddeposito` int(11) NOT NULL,
  `iddomicilio` int(11) NOT NULL,
  `idcliente` int(11) NOT NULL,
  `nota_num` int(11) NOT NULL,
  `cliente` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_pedido` date NOT NULL,
  `kilos` decimal(11,2) NOT NULL,
  `tipo_pastel` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `relleno` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `sabor` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `cubierto` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `envinado` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `color` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `escrito` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `tipo_decorado` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `detalles_dec` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `costo_dec` decimal(11,2) NOT NULL,
  `fecha_entrega` date NOT NULL,
  `hora_entrega` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `direccion` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `total_pedido` decimal(11,2) NOT NULL,
  `anticipo` int(20) DEFAULT NULL,
  `resta` int(20) DEFAULT NULL,
  `estado` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`idpedido`, `iddeposito`, `iddomicilio`, `idcliente`, `nota_num`, `cliente`, `telefono`, `fecha_pedido`, `kilos`, `tipo_pastel`, `relleno`, `sabor`, `cubierto`, `envinado`, `color`, `escrito`, `tipo_decorado`, `detalles_dec`, `costo_dec`, `fecha_entrega`, `hora_entrega`, `direccion`, `total_pedido`, `anticipo`, `resta`, `estado`) VALUES
(22, 1, 3, 2, 1000, 'Juan', '9292', '2017-05-07', '1.00', 'Normal', 'No', 'Vainilla', 'Merengue', 'Si', 'jskjskjs', 'sjkskjs', 'Sencillo', 'kslkskls', '0.00', '2017-05-27', '01:00', 'ddd', '880.00', 700, 180, 'Pendiente'),
(23, 1, 1, 1, 1500, 'Suriel', '2721027729', '2017-05-07', '2.00', 'Normal', 'Piña', 'Vainilla', 'Merengue', 'Si', 'Azul', 'Hola Mundo', 'Sencillo', 'JUgadores Soccer', '50.00', '2017-05-21', '05:00', 'Ninguna', '1300.00', 1000, 300, 'Pendiente'),
(24, 2, 4, 4, 678, 'Eduardo', '727272', '2017-05-12', '4.00', 'Normal', 'Durazno', 'Chocolate', 'Chantilli', 'Si', 'rosado', 'Felicidades', 'Sencillo', 'XV años', '100.00', '2017-05-19', '10:00', 'SALON', '3510.00', 3000, 510, 'Pendiente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idproducto` int(11) NOT NULL,
  `idcategoria` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `descripcion` varchar(512) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `imagen` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `precio_venta` decimal(11,2) NOT NULL,
  `estado` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`idproducto`, `idcategoria`, `nombre`, `stock`, `descripcion`, `imagen`, `precio_venta`, `estado`) VALUES
(1, 1, 'Ninguno', 0, 'Sin descripcion', 'gelatina-de-frutas.png', '0.00', 'Activo'),
(2, 1, 'Gelatina China', 17, 'Sabor Rompope', 'gelatinaRoscaDurazno.png', '90.00', 'Activo'),
(3, 1, 'Concha', 11, 'Sabor Vinilla', 'ubuntu-miniatura3.jpg', '20.00', 'Activo'),
(4, 2, 'Panques', 15, 'Nuez', 'gelatinaRoscaDurazno.png', '8.00', 'Activo'),
(5, 2, 'Pasteles Xv años', 2, 'Pastele para la fiesta', 'sorpresa.jpg', '3000.00', 'Inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `idproveedor` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `direccion` varchar(70) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `estado` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`idproveedor`, `nombre`, `direccion`, `telefono`, `email`, `estado`) VALUES
(1, 'Gonzalo', 'Orizaba', '2721231212', 'gonzalo@gmail.com', 'Inactivo'),
(2, 'Alfredo', 'Cordoba', '2721231212', 'alfredo@gmail.com', 'Activo'),
(3, 'Alonso', 'Orizaba', '123123123', 'alonso@gmail.com', 'Activo'),
(4, 'Jaime', 'Nogales', '2828282828', 'jaime@gmail.com', 'Inactivo'),
(5, 'Miguel', 'Rio Blanco', '2727272727', NULL, 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `admin`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Asael', 0, 'kato_1350@hotmail.com', '$2y$10$ABskYVaWpM3EivJtJ0FhW.sdu0IdpDX4kDcQtTmTNhQpZbmZcQ1za', 'UmOdTGEGBbxP7xTQTFfJ9gqZK3c9kJpYzoovRlWtUPz77QarM5A3UcJybLB0', '2017-04-20 01:40:38', '2017-04-20 01:40:38'),
(2, 'Suriel', 1, 'suriel@admin.com', '$2y$10$xds.90LFfY0KNrVtjEhQ7.8JbrzPw/RlGodBN7dYEHmXJBkBhR8xq', 'b69rZ2Q7xtwU3JQCHhvUf9TKY6xWfZF8NkEmN8KG1EVsxEYKUlEvPA1uakiC', '2017-04-20 01:41:42', '2017-04-20 02:07:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `idventa` int(11) NOT NULL,
  `fecha_hora` datetime NOT NULL,
  `total_venta` decimal(11,2) NOT NULL,
  `estado` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`idventa`, `fecha_hora`, `total_venta`, `estado`) VALUES
(1, '2017-05-07 20:28:43', '260.00', 'C'),
(2, '2017-05-07 20:41:55', '130.00', 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id` int(10) NOT NULL DEFAULT '0',
  `productos` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `cantidades` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `total` float(100,2) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id`, `productos`, `cantidades`, `total`, `updated_at`, `created_at`) VALUES
(0, 'Concha', '2', 40.00, '2017-05-04 07:05:43', '2017-05-04 07:05:43');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`idcategoria`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idcliente`);

--
-- Indices de la tabla `depositoBase`
--
ALTER TABLE `depositoBase`
  ADD PRIMARY KEY (`iddeposito`);

--
-- Indices de la tabla `detalle_pedido_producto`
--
ALTER TABLE `detalle_pedido_producto`
  ADD PRIMARY KEY (`iddetalle_pedido_producto`),
  ADD KEY `idpedido` (`idpedido`),
  ADD KEY `idproducto` (`idproducto`);

--
-- Indices de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD PRIMARY KEY (`iddetalle_venta`),
  ADD KEY `idventa` (`idventa`),
  ADD KEY `idproducto` (`idproducto`);

--
-- Indices de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  ADD PRIMARY KEY (`iddomicilio`);

--
-- Indices de la tabla `materiaPrima`
--
ALTER TABLE `materiaPrima`
  ADD PRIMARY KEY (`idmateriaPrima`),
  ADD KEY `idproveedor` (`idproveedor`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`idpedido`),
  ADD KEY `iddeposito` (`iddeposito`),
  ADD KEY `iddomicilio` (`iddomicilio`),
  ADD KEY `idcliente` (`idcliente`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idproducto`),
  ADD KEY `fk_producto_categoria_idx` (`idcategoria`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`idproveedor`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`idventa`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `idcategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `depositoBase`
--
ALTER TABLE `depositoBase`
  MODIFY `iddeposito` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `detalle_pedido_producto`
--
ALTER TABLE `detalle_pedido_producto`
  MODIFY `iddetalle_pedido_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  MODIFY `iddetalle_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  MODIFY `iddomicilio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `materiaPrima`
--
ALTER TABLE `materiaPrima`
  MODIFY `idmateriaPrima` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pedido`
--
ALTER TABLE `pedido`
  MODIFY `idpedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `idproducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `idproveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `idventa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detalle_pedido_producto`
--
ALTER TABLE `detalle_pedido_producto`
  ADD CONSTRAINT `detalle_pedido_producto_ibfk_1` FOREIGN KEY (`idpedido`) REFERENCES `pedido` (`idpedido`),
  ADD CONSTRAINT `detalle_pedido_producto_ibfk_2` FOREIGN KEY (`idproducto`) REFERENCES `producto` (`idproducto`);

--
-- Filtros para la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD CONSTRAINT `detalle_venta_ibfk_1` FOREIGN KEY (`idventa`) REFERENCES `venta` (`idventa`),
  ADD CONSTRAINT `detalle_venta_ibfk_2` FOREIGN KEY (`idproducto`) REFERENCES `producto` (`idproducto`);

--
-- Filtros para la tabla `materiaPrima`
--
ALTER TABLE `materiaPrima`
  ADD CONSTRAINT `materiaPrima_ibfk_1` FOREIGN KEY (`idproveedor`) REFERENCES `proveedor` (`idproveedor`);

--
-- Filtros para la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `pedido_ibfk_1` FOREIGN KEY (`iddeposito`) REFERENCES `depositoBase` (`iddeposito`),
  ADD CONSTRAINT `pedido_ibfk_2` FOREIGN KEY (`iddomicilio`) REFERENCES `domicilio` (`iddomicilio`),
  ADD CONSTRAINT `pedido_ibfk_3` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`);

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_producto_categoria` FOREIGN KEY (`idcategoria`) REFERENCES `categoria` (`idcategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
